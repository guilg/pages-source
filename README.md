# pages-source

Source for [Codeberg pages](https://codeberg.page/), served on [https://guilg.codeberg.page](https://guilg.codeberg.page).
Powered by Zola with the [academic-theme](https://github.com/zola-academic/zola-academic), modified to be able to specify full HSL colors and using local css and forkawesome instead of CDN.
