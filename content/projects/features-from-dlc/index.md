+++
title = "features-from-dlc"
description = "A Python package to quantify DeepLabCut tracking data"
template = "post.html"
date = 2025-01-25

[extra]
links = [
    { name = "Repo", link = "https://github.com/TeamNCMC/features-from-dlc" },
    { name = "Doc", link = "https://github.com/TeamNCMC/features-from-dlc/blob/main/README.md" }
]
authors = ["Guillaume Le Goc"]
+++

After pose estimation of an animal in video recordings with [DeepLabCut](https://github.com/DeepLabCut/DeepLabCut), this package is used to :
* pool files by subjects & conditions,
* compute user-defined behavioral features, quantifying during-stimulation metrics and response delay,
* perform statistical significance tests on those metrics,
* display the averaged time series and metrics.

This package was developed at [NeuroPSI](https://neuropsi.cnrs.fr/en/departments/icn/group-leader-julien-bouvier/).

A neuronal population of interest is specifically targeted with optogenetic stimulations while the mouse is freely behaving and video-monitored. Long recordings are [broken down](https://github.com/TeamNCMC/videocutter) into single stimulus trials and relevant body parts are tracked with DeepLabCut.

The `features-from-dlc` package is subsequently used to extract behavioral features (such as speed, turning angle...) and plot time series averaged accross conditions (distinct neuronal population or stimulation intensity), as well as quantitative metrics measuring change during stimulation. Furthermore, statistical significance tests are performed between groups.