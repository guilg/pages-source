+++
title = "arduino-temperature-control"
description = "A GUI to control temperature with Peltiers and Arduino"
template = "post.html"
date = 2021-10-01

[extra]
links = [
    { name = "Repo", link = "https://codeberg.org/guilg/Arduino-temperature-control" },
    { name = "Doc", link = "https://codeberg.org/guilg/Arduino-temperature-control/src/branch/master/README.md" }
]
authors = ["Guillaume Le Goc"]
image = false
+++

A Python graphical user interface (GUI) to control an Arduino board connected to a temperature sensor (thermocouple) and Peltier elements. Arduino code and electronics design are also provided.

Developped in the [Laboratoire Jean Perrin](https://www.labojeanperrin.fr/?lang=en) to control larval zebrafish bath temperature during whole-brain lightsheet imaging.