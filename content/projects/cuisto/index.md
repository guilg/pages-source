+++
title = "cuisto"
description = "A Python package to quantify histological data"
template = "post.html"
date = 2025-01-25

[extra]
links = [
    { name = "Repo", link = "https://github.com/TeamNCMC/cuisto" },
    { name = "Doc", link = "https://teamncmc.github.io/cuisto/" }
]
authors = ["Guillaume Le Goc"]
+++

This package aims at using the output of the image analysis software [QuPath](https://qupath.github.io), that  allows for objects detections in large images.

After analysis in QuPath, `cuisto` is used to collect the data from different subjects, pool them based on the regions of interest names and derive quantifying metrics from the raw count of biological objects. Those metrics include, for each regions :
* the raw measurement,
* the areal density, eg. the raw measurement divided by  the region aera,
* the relative density, eg. the areal density as a fraction of the total density.
Furthermore, `cuisto` can leverage the atlas coordinates of each object of interest to compute and display spatial distributions in the reference atlas space.

The [extensive documentation](https://teamncmc.github.io/cuisto/) provides installation and usage instructions, as well as guides to use QuPath in the scope of counting objects of interest in large images of histological slices.

It was developed at [NeuroPSI](https://neuropsi.cnrs.fr/en/departments/icn/group-leader-julien-bouvier/).