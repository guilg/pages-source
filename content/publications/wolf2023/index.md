+++

template = "post.html"
title = "Emergence of time persistence in a data-driven neural network model"
date = 2023-03-14

[extra]
abstract = """Establishing accurate as well as interpretable models of network activity is an open challenge in systems neuroscience. Here, we infer an energy-­based model of the anterior rhombencephalic turning region (ARTR), a circuit that controls zebrafish swimming statistics, using functional recordings of the spontaneous activity of hundreds of neurons. Although our model is trained to reproduce the low-­order statistics of the network activity at short time scales, its simulated dynamics quantitatively captures the slowly alternating activity of the ARTR. It further reproduces the modulation of this persistent dynamics by the water temperature and visual stimulation. Mathematical analysis of the model unveils a low-d­ imensional landscape-­based representation of the ARTR activity, where the slow network dynamics reflects Arrhenius-­like barriers crossings between metastable states. Our work thus shows how data-d­ riven models built from large neural populations recordings can be reduced to low-­dimensional functional models in order to reveal the fundamental mechanisms controlling the collective neuronal dynamics."""

authors = ["S. Wolf*, G. Le Goc*, G. Debrégeas, S. Cocco, R. Monasson"]

links = [
    { name = "PDF", link = "/papers/wolf2023.pdf" },
    { name = "DOI", link = "https://doi.org/10.7554/eLife.79541" }
]

publication = "eLife"
year = 2023


+++

