+++

template = "post.html"
title = "Thermal modulation of Zebrafish exploratory statistics reveals constraints on individual behavioral variability"
date = 2021-09-21

[extra]
abstract = """Background: Variability is a hallmark of animal behavior. It contributes to survival by endowing individuals and populations with the capacity to adapt to ever-changing environmental conditions. Intra-individual variability is thought to reflect both endogenous and exogenous modulations of the neural dynamics of the central nervous system. However, how variability is internally regulated and modulated by external cues remains elusive. Here, we address this question by analyzing the statistics of spontaneous exploration of freely swimming zebrafish larvae and by probing how these locomotor patterns are impacted when changing the water temperatures within an ethologically relevant range.
Results: We show that, for this simple animal model, five short-term kinematic parameters — interbout interval, turn amplitude, travelled distance, turn probability, and orientational flipping rate — together control the long-term exploratory dynamics. We establish that the bath temperature consistently impacts the means of these parameters, but leave their pairwise covariance unchanged. These results indicate that the temperature merely controls the sampling statistics within a well-defined kinematic space delineated by this robust statistical structure. At a given temperature, individual animals explore the behavioral space over a timescale of tens of minutes, suggestive of a slow internal state modulation that could be externally biased through the bath temperature. By combining these various observations into a minimal stochastic model of navigation, we show that this thermal modulation of locomotor kinematics results in a thermophobic behavior, complementing direct gradient-sensing mechanisms.
Conclusions: This study establishes the existence of a well-defined locomotor space accessible to zebrafish larvae during spontaneous exploration, and quantifies self-generated modulation of locomotor patterns. Intra-individual variability reflects a slow diffusive-like probing of this space by the animal. The bath temperature in turn restricts the sampling statistics to sub-regions, endowing the animal with basic thermophobicity. This study suggests that in zebrafish, as well as in other ectothermic animals, ambient temperature could be used to efficiently manipulate internal states in a simple and ethological way."""

authors = ["G. Le Goc, J. Lafaye, S. Karpenko, V. Bormuth, R. Candelier, G. Debrégeas"]

links = [
    { name = "PDF", link = "/papers/legoc2021.pdf" },
    { name = "DOI", link = "https://doi.org/10.1186/s12915-021-01126-w" }
]

publication = "BMC Biology"
year = 2021


+++

