+++

template = "post.html"
title = "Magnetic actuation of otoliths allows behavioral and brain-wide neuronal exploration of vestibulo-motor processing in larval zebrafish"
date = 2023-06-01

[extra]
abstract = """The vestibular system in the inner ear plays a central role in sensorimotor control by informing the brain about the orientation and acceleration of the head. However, most experiments in neurophysiology are performed using head-ﬁxed conﬁgurations, depriving animals of vestibular inputs. To overcome this limitation, we decorated the utricular otolith of the vestibular system in larval zebraﬁsh with paramagnetic nanoparticles. This procedure effectively endowed the animal with magneto-sensitive capacities: applied magnetic ﬁeld gradients induced forces on the otoliths, resulting in robust behavioral responses comparable to those evoked by rotating the animal by up to 25 . We recorded the whole-brain neuronal response to this ﬁctive motion stimulation using light-sheet functional imaging. Experiments performed in unilaterally injected ﬁsh revealed the activation of a commissural inhibition between the brain hemispheres. This magnetic-based stimulation technique for larval zebraﬁsh opens new perspectives to functionally dissect the neural circuits underlying vestibular processing and to develop multisensory virtual environments, including vestibular feedback."""

authors = ["N. Beiza Canelo, H. Moulle, T. Pujol, T. Panier, G. Migault, G. Le Goc, P. Tapie, N. Desprat, H. Straka, G. Debrégeas, V. Bormuth"]

links = [
    { name = "PDF", link = "/papers/beiza2023.pdf" },
    { name = "DOI", link = "https://doi.org/10.1016/j.cub.2023.05.026" }
]

publication = "Current Biology"
year = 2023


+++

