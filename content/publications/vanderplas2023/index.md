+++

template = "post.html"
title = "Neural assemblies uncovered by generative modeling explain whole-brain activity statistics and reflect structural connectivity"
date = 2023-01-17

[extra]
abstract = "Patterns of endogenous activity in the brain reflect a stochastic exploration of the neuronal state space that is constrained by the underlying assembly organization of neurons. Yet, it remains to be shown that this interplay between neurons and their assembly dynamics indeed suffices to generate whole-­brain data statistics. Here, we recorded the activity from ∼40,000 neurons simultaneously in zebrafish larvae, and show that a data-­driven generative model of neuron-­assembly interactions can accurately reproduce the mean activity and pairwise correlation statistics of their spontaneous activity. This model, the compositional Restricted Boltzmann Machine (cRBM), unveils ∼200 neural assemblies, which compose neurophysiological circuits and whose various combinations form successive brain states. We then performed in silico perturbation experiments to determine the interregional functional connectivity, which is conserved across individual animals and correlates well with structural connectivity. Our results showcase how cRBMs can capture the coarse-­grained organization of the zebrafish brain. Notably, this generative model can readily be deployed to parse neural data obtained by other large-­scale recording techniques."
authors = ["T. van der Plas*, J. Tubiana*, G. Le Goc, G. Migault, M. Kunst, H. Baier, V. Bormuth, B. Englitz, G. Debrégeas"]
publication = "eLife"
year = 2023

links = [
    { name = "PDF", link = "/papers/vanderplas2023.pdf" },
    { name = "DOI", link = "https://doi.org/10.7554/eLife.83139"}
]

+++

