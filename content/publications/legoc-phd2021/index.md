+++

template = "post.html"
title = "Exploration of larval zebrafish internal states using temperature"
date = 2021-10-01

[extra]
abstract = "Animals have to continuously adapt to their environment. Spontaneous behavior is thus modulated by sensory cues extracted from its environment, but shows an intrinsic variability, both between individuals and within individuals. The regulation of the internal state that defines the observed behavior at each moment, and how it is modulated by environmental conditions, remains elusive. In this thesis, we use zebrafish larva, a vertebrate amenable to whole-brain imaging, to probe these internal dynamics. Temperature allows the modulation of individual internal dynamics and serves as a lever for the study of variability in this animal. A behavioral study allowed us to establish how the paramaters that control fish navigation were dependent on both absolute temperature and its temporal variation, allowing the animal to confine itself to a temperature range of 22--28°C. We also mapped whole-brain neural response to brief thermal stimuli. These demonstrate that regions involved in processing hot and cold signals are distinct and both widely distributed throughout the brain. In a second part, we focused on exploratory behavior and its variability for larvae swimming in water at different temperatures without thermal gradient. We showed that the swimming dynamics is constrained within a fixed space defined by the statistical structure of the different kinematic parameters, a space probed by the animal over several tens of minutes. We then focused on a neural circuit controlling the orientation of the swim (right vs left), whose activity shows a persistence property over long times. We showed that behavioral and neural orientational persistence times evolve consistently with bath temperature. These neural recording data were used to train a network model (Ising model) able to reproduce the observed neural dynamics. In conclusion, this work has provided a quantitative description of the spontaneous exploratory dynamics of larval zebrafish, its stochastic and variable character, and the influence of temperature on this behavior. Functional imaging, coupled with neural circuit modeling, has allowed us to shed light on some of the neural mechanisms behind these oberservations. "
authors = ["G. Le Goc"]
publication = "Ph.D. thesis, Sorbonne Université"
year = 2021

links = [
    { name = "PDF (fr)", link = "/phd/manuscript.pdf" },
    { name = "Slides (en)", link = "/phd/slides/index.html"},
    { name = "DOI", link = "https://hal.science/tel-03404623v2"}
]

+++

