#!/bin/bash

zola build
rsync -rcv ./public/ ../pages
cd ../pages
git add --all
git commit -m "Updated with script"
git push
